package com.example.vo;


import lombok.Data;

@Data
public class MessageVo {
    int uuid;
    String value;
    long startime;

}

package com.example.vo;

import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;

public class MessageVoSerializer implements Serializer<MessageVo> {

    @Override
    public byte[] serialize(String topic, MessageVo data) {
        if (data == null) {
            return null;
        }
        return data.toString().getBytes(StandardCharsets.UTF_8);
    }
}
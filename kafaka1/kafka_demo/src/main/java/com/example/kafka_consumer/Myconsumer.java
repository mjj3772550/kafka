package com.example.kafka_consumer;

import com.example.kafka_producer.Producer;
import com.example.vo.MessageVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.DataInput;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@Service
public class Myconsumer {

    @Autowired
    Producer producer;


    public void getMessage(){
        long startime;
        long endtime;
        ObjectMapper objectMapper = new ObjectMapper();
//         1. 创建Kafka生产者的配置对象
        Properties properties = new Properties();
        // 2. 给Kafka配置对象添加配置信息：bootstrap.servers
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.2.12:9092");
        properties.setProperty("group.id","demo");
        properties.setProperty("enable.auto.commit", "true");
        properties.setProperty("auto.commit.interval.ms", "1000");
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Arrays.asList("test"));


        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
            System.out.println("进行中");
            if (records.count() != 0) {
                for (ConsumerRecord<String, String> record : records) {

                    MessageVo messageVo=null;
                    try {
//                        messageVo= (MessageVo) Class.forName("com.example.vo.MessageVo").cast(record.value());
                        String value = record.value();
                        System.out.println(value);
                        messageVo= objectMapper.readValue(value, MessageVo.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(1525==messageVo.getUuid()){
                        System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
                        startime=messageVo.getStartime();
                        endtime=System.currentTimeMillis();
                        System.out.println("结束时间:"+endtime+"ms");
                        System.out.println("=========================================================================");
                        System.out.println("延迟为"+(endtime-startime)+"ms");
                    }
                }


            }
//            consumer.commitAsync();
        }

    }

}

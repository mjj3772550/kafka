package com.example.kafka_consumer;

import com.example.kafka_producer.Producer;
import com.example.vo.MessageVoSerializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

@Service
public class Consumer {

    public ArrayList getMessage(KafkaConsumer<String, String> consumer)  {
        ArrayList messagelist=new ArrayList<>();
        boolean flag=true;

        //消费者订阅主题
        consumer.subscribe(Arrays.asList("test"));
        //循环等待消息
        while (flag){
            System.out.println("进行中");
            //换监听机制
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            if (records.count() !=0 ){
                for (ConsumerRecord<String, String> record : records){
                    //System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
                    System.out.printf("offset = %d, value = %s%n", record.offset(), record.value());
                    messagelist.add(record.value());
                }
                flag=false;
            }
            consumer.commitSync();
        }
        //consumer.close();
        return messagelist;
    }


//        @KafkaListener(topics = "test",groupId = "demo")
//        public void getMessage(ConsumerRecords<?, ?> record){
//            Properties properties = new Properties();
//            // 2. 给Kafka配置对象添加配置信息：bootstrap.servers
//            properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServers);
//            properties.put(ConsumerConfig.GROUP_ID_CONFIG,"demo");
//            properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, autoCommit);
//            properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, key);
//            properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, value);
//            KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
//            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
//
//
//            System.out.println(messageVO.getMessage()+"\t"+messageVO.getTime());
//            System.out.println(record.);
//        }



//@Component
//public class Consumer {
//    @KafkaListener(topics = "test1",groupId = "demo")
//    public void OnMeseeage(ConsumerRecords<?, ?> record, Acknowledgment ack, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic){
//        System.out.println("消息："+record.topic()+record.value());
//        //手动提交
//        ack.acknowledge();
//    }
}

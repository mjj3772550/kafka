package com.example.kafka_consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public class KafkaListenerManualCommit {
    @KafkaListener(topics = "test",groupId = "demo")
    public static void getMessage(ConsumerRecords<String, String> record, Acknowledgment ack){
        //通过linstner
        System.out.println("监听到消息：");
        ack.acknowledge();
    }
}

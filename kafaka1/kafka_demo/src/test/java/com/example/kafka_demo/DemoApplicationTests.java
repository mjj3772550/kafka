package com.example.kafka_demo;


import com.example.kafka_consumer.Consumer;
import com.example.kafka_consumer.Myconsumer;
import com.example.kafka_producer.Producer;
import com.example.vo.MessageVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Properties;
import java.util.UUID;



    @SpringBootTest
    class DemoApplicationTest {
        @Autowired
        private Producer producer;
        @Autowired
        private Consumer consumer;
        //        @Autowired
//        private MessageVO messageVO;
        long startTime;
        long endTime;
//        @Value("${spring.kafka.bootstrap-servers}")
//        private String bootstrapServers;
//
//        @Value("${spring.kafka.consumer.enable-auto-commit}")
//        private String autoCommit;
//        @Value("${spring.kafka.consumer.auto-commit-interval}")
//        private String interval;
//        @Value("${spring.kafka.consumer.key-deserializer}")
//        private String key;
//        @Value("${spring.kafka.consumer.value-deserializer}")
//        private String value;
        //@Autowired
        //private MessageVO messageVO;


        //		@Test
//		void testSendMessage() throws JsonProcessingException {
//
//			String message = "message";
//			messageVO.setMessage(message);
//			messageVO.setId(0);
//			messageVO.setTime(System.currentTimeMillis());
//			//发送消息
//
//			producer.sendMessage(messageVO);
//	}


        @Test
        void testSendMessage() throws JsonProcessingException {

            String message = "message";
            //发送消息
            producer.sendMessage(message);
        }


        @Test
        void testMessage()  {

//			//for (long i = 1; i <=10; i++) {
//				String message = "kafka";
//				messageVO.setMessage(message);
//				messageVO.setId(1);
//				messageVO.setTime(System.currentTimeMillis());
//				//发送消息
//				producer.sendMessage(messageVO);
//				//消息被消费
//				MessageVO overmessage=consumer.getMessage();
//				endTime = System.currentTimeMillis();
//				System.out.println("发送时间: " + (endTime - overmessage.getTime()) + "ms");
//			//}
            // 1. 创建Kafka生产者的配置对象
//            Properties properties = new Properties();
//            // 2. 给Kafka配置对象添加配置信息：bootstrap.servers
//            properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServers);//配置kafka地址
//            properties.put(ConsumerConfig.GROUP_ID_CONFIG,"demo");//配置消费者组
//            properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, autoCommit);//配置自动提交
//            properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, key);//键反序列化
//            properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, value);//值反序列化
            Properties properties = new Properties();
            // 2. 给Kafka配置对象添加配置信息：bootstrap.servers
            properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.126.1:9092");
            properties.setProperty("group.id","demo");
            properties.setProperty("enable.auto.commit", "false");
            properties.setProperty("auto.commit.interval.ms", "1000");
            properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            KafkaConsumer<String, String> consumer1 = new KafkaConsumer<>(properties);


            for (int i = 0; i < 50; i++) {
                //System.out.println("第"+i+"次发送");
                startTime =System.currentTimeMillis();
                producer.sendMessage(String.valueOf(System.currentTimeMillis()));
                ArrayList messagelist=consumer.getMessage(consumer1);
                startTime= Long.parseLong((String) messagelist.get(0));
                //System.out.println("第"+i+"次获取");
                endTime = System.currentTimeMillis();
                //System.out.println("开始时间："+startTime+"结束时间：" + endTime);
                System.out.println("发送时间: " + (endTime - startTime) + "ms");
            }



        }

        @Test
        void testOverMessage(){
            //consumer.getMessage();
            //吞吐量：测大量消息的延迟,除以消息数量,每秒处理的消息数量
        }


    @Test
    void testSendMessage1() {
        ObjectMapper objectMapper = new ObjectMapper();
        MessageVo messageVo = new MessageVo();
        String uuid = "";
        String jsonString = "";
        for (int i = 0; i < 1000; i++) {
            uuid = UUID.randomUUID().toString();
            messageVo.setUuid(i);
            messageVo.setValue("message" + i);
            long l = System.currentTimeMillis();
            messageVo.setStartime(l);
//            System.out.println(l);
            try {
                jsonString = objectMapper.writeValueAsString(messageVo);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            producer.sendMessage(jsonString);
        }





        //}

//        System.out.println("开始时间："+startTime+"结束时间："+endTime);
//        System.out.println("发送时间: " + (endTime - startTime) + "ms");
    }

    @Test
    void receiveSendMessage()  {
//        endtime=System.currentTimeMillis();
//        System.out.println(endtime-starttime);
//        consumer.getMessage();
    }

    @Test
    void uuid(){
        UUID uuid = UUID.randomUUID();
        System.out.println(uuid);

    }
}
